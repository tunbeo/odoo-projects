import numpy as np
import glob
from PIL import Image

im = Image.open('fig1.png')
data = np.array(im)


print(glob.glob("*.png"))

#red, green, blue = data[:,:,0], data[:,:,1], data[:,:,2]
#mask = (red == r1) & (green == g1) & (blue == b1)
#data[:,:,:3][mask] = [r2, g2, b2]

def change_color(r1, g1, b1, r2, g2, b2):
    red, green, blue = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    data[:, :, :3][mask] = [r2, g2, b2]
    return data

r1, g1, b1 = 46, 14, 255 # XINGFA BLUE
r2, g2, b2 = 4, 33, 52 # Topal blue
data = change_color(r1, g1, b1, r2, g2, b2)

r1, g1, b1 = 25, 43, 255 # XINGFA BLUE
r2, g2, b2 = 4, 33, 52 # Topal blue
data = change_color(r1, g1, b1, r2, g2, b2)


r1, g1, b1 = 0, 0, 255 # BLUE
r2, g2, b2 = 4, 33, 52 # Topal blue
data = change_color(r1, g1, b1, r2, g2, b2)

r1, g1, b1 = 255, 0, 0 # RED
r2, g2, b2 = 4, 33, 52 # Topal blue
data = change_color(r1, g1, b1, r2, g2, b2)

r1, g1, b1 = 0, 255, 255 # xanh navy org
r2, g2, b2 = 191, 191, 191 # grey
data = change_color(r1, g1, b1, r2, g2, b2)




im = Image.fromarray(data)
im.save('fig1_modified.png')