# -*- coding: utf-8 -*-
{
    'name': 'anv_system',
    'version': '0.1',
    'summary': 'ANV SYSTEM module',
    'sequence': -100,
    'description': """App System tính toán cắt nhôm kính ANV""",
    'category': 'Uncategorized',
    'author': 'Ứng dụng ngành nhôm',
    'website': 'https://topal.vn',
    'depends': ['base','website','mail','website', 'adg'],
    'data': [        
    ],
    'demo': [        
    ],
    'qweb': [],
    'images': ['static/description/banner.gif'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
