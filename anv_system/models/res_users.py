# -*- coding: utf-8 -*-
from odoo import fields, models, _


class User(models.Model):
    _inherit = 'res.users'

    anvaddress_full = fields.Char(string='Address full', related='anvaddress_id.address_full')
