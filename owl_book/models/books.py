from odoo import fields, models, api


class BookBooks(models.Model):
    _name = 'book.books'
    _description = 'Books'
    _rec_name = 'title'

    title = fields.Char()
    price = fields.Float()
    pages_number = fields.Integer()
    kind = fields.Char()
    url = fields.Char()
    author_id = fields.Many2one('res.partner')
    store_id = fields.Many2one('book.store')
