from odoo import fields, models, api


class BookStore(models.Model):
    _name = 'book.store'
    _description = 'Stores'
    _rec_name = 'name'

    name = fields.Char()
