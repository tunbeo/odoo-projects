from odoo import http
import json


class Hello(http.Controller):

    def get_books_list(self):
        books = http.request.env['book.books'].sudo().search([])
        list_books = []
        for book in books:
            list_books.append({
                'title': book.title,
                'price': book.price,
                'pages_number': book.pages_number,
                'kind': book.kind,
            })
        return list_books

    @http.route('/books', auth='public', website=True)
    def index(self):
        values = {'data': self.get_books_list()}
        return http.request.render('owl_book.index', values)

    @http.route('/books/list/data', auth='public', type='http')
    def book_list_json(self):
        """ Get books data in a JSON format """
        values = {'result': self.get_books_list()}
        return json.dumps(values)
