odoo.define('owl_book/static/src/components/search/search.js', function(require){

'use strict';

const { Component } = owl;
const { useState } = owl.hooks;

class Search extends Component {

    constructor(parent, props) {
        super(parent, props);
        console.log('Loading placeholder...')
    }

}

Object.assign(Search, {
    defaultProps: {
        class: 'search-box'
    },
    template: 'owl_book.Search'
});

return Search;

})
