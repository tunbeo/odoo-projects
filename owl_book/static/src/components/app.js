odoo.define('owl_book/static/src/components/app.js', function(require) {

'use strict';

const components = {
    List: require('owl_book/static/src/components/list/list.js'),
};


const { Component } = owl;
const { useState } = owl.hooks;

class App extends Component {

    constructor() {
        super(...arguments);
        this.state = {books: new components.List().listBooks}
    }
}

Object.assign(App, {
    components,
    template: 'owl_book.App',
});

return App;

})
