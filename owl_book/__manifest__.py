{
    'name': "OWL Book",
    'summary': """Book Finder App: user can search, edit and delete books 
    registered in Odoo backend. We can also add a link to view more detail 
    about this book on Amazon. Can also add loading animations.""",
    'depends': [
        'base',
        'portal',
        'website',
    ],
    'data': [
        'views/assets.xml',
        'views/index.xml',
        'views/books.xml',
        'views/store.xml',
        # security
        'security/groups.xml',
        'security/ir.model.access.csv',
    ],
    'qweb': [
        'static/src/components/templates.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
