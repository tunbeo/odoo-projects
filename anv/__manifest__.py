# -*- coding: utf-8 -*-
{
    'name': 'anv',
    'version': '0.1',
    'summary': 'ANV module',
    'sequence': -100,
    'description': """App tính toán cắt nhôm kính cho công ty Topal""",
    'category': 'Uncategorized',
    'author': 'Ứng dụng ngành nhôm',
    'website': 'https://topal.vn',
    'depends': ['base','website','mail','website', 'adg'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/profilecategory.xml',
        'views/construction.xml',
        'views/door.xml',
        'views/address_book_views.xml',
        'views/res_config_settings_views.xml',
        'views/catalog.xml',
        'data/address.book.csv',
        'data_px/address.book.csv',
        'views/anv_inherit.xml',
    ],
    'demo': [
        'demo/demo.xml'
    ],
    'qweb': [],
    'images': ['static/description/banner.gif'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
