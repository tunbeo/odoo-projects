# -*- coding: utf8 -*-
import json
import math

from odoo import http, tools
from odoo.http import content_disposition, dispatch_rpc, request, Response
import requests
from requests import Response

from urllib import request

import babel.messages.pofile
import base64
import copy
import datetime
import functools
import glob
import hashlib
import io
import itertools
import jinja2
import json
import logging
import operator
import os
import re
import sys
import tempfile

import werkzeug
import werkzeug.exceptions
import werkzeug.utils
import werkzeug.wrappers
import werkzeug.wsgi
from collections import OrderedDict, defaultdict, Counter
from werkzeug.urls import url_encode, url_decode, iri_to_uri
from lxml import etree
import unicodedata


import odoo
import odoo.modules.registry
from odoo.api import call_kw, Environment
from odoo.modules import get_module_path, get_resource_path
from odoo.tools import image_process, topological_sort, html_escape, pycompat, ustr, apply_inheritance_specs, lazy_property, float_repr
from odoo.tools.mimetypes import guess_mimetype
from odoo.tools.translate import _
from odoo.tools.misc import str2bool, xlsxwriter, file_open
from odoo.tools.safe_eval import safe_eval, time
from odoo import http, tools
from odoo.http import content_disposition, dispatch_rpc, request, serialize_exception as _serialize_exception, Response
from odoo.exceptions import AccessError, UserError, AccessDenied
from odoo.models import check_method_name
from odoo.service import db, security


class AnvControllers(http.Controller):
    # get list data anv/profilecategory
    @http.route("/anv/profilecategory", auth='public', type='json')
    def method_get_patients(self, **val):
        profilecategory = http.request.env['anv.profilecategory'].sudo().search([])
        list_profile = []
        for i in profilecategory:
            vals = {
                'id': i.id,
                'name': i.name,
            }
            list_profile.append(vals)

        data = {'status': 200, 'response': list_profile, 'message': 'Danh sách ngành nhôm'}
        return data

    @http.route("/anv/login1", auth='none', type='json')
    def method_login(self, **val):
        db = val['db'],
        mobile = val['mobile'],
        password = val['password'],
        list_user = http.request.env['res.users'].sudo().search([('mobile', '=', mobile)], limit= 1)
        login = ''
        for i in list_user:
            login = i.login

        if login != '':
            request.session.authenticate(db, login, password)
            return request.env['ir.http'].session_info()
        else:

            return {'status': 300, 'message': 'không có user'}

    @http.route('/anv/authenticate', type='json', auth="none")
    def authenticate(self, db, login, password, base_location=None):
        email = ''
        list_user = http.request.env['res.users'].sudo().search([('anvmobile', '=', login)], limit=1)
        for i in list_user:
            email = i.login

        request.session.authenticate(db, email, password)
        return request.env['ir.http'].session_info()

    # lay danh sach cong trinh theo user
    #@http.route("/anv/profilecategory", auth='public', type='json')
    @http.route("/anv/construction", auth='public', type='json')
    def method_construction_gets(self, **val):
        list_const = http.request.env['anv.construction'].sudo().search([])
        data_cons = []
        for i in list_const:
            vals = {
                'id': i.id,
                'name': i.name,
                'address': i.address,
                'owner': i.owner,
                'mobile': i.mobile,
                'notes': i.notes,
                'sequence': i.sequence,
                'active1': i.active1
            }
            data_cons.append(vals)

        data = {'status': 200, 'response': data_cons, 'message': 'danh sách công trình'}
        return data

    # them moi cong trinh
    @http.route("/anv/construction_insert", auth='user', type='json')
    def construction_insert(self, **params):
        try:
            if params['name']:
                vals = {
                    'name': params['name'],
                    'address': params['address'],
                    'owner': params['owner'],
                    'mobile': params['mobile'],
                    'notes': params['notes'],
                    'sequence': params['sequence'],
                    'active1': params['active1'],
                    'address_id': params['address_id'],
                }
                new_item = http.request.env['anv.construction'].sudo().create(vals)
                data = {'success': True, 'message': 'Tạo mới hoàn thành', 'id': new_item.id}
            return data
        except Exception as e:
            return {'success': False, "error": e, 'message': 'Tạo mới thất bại'}

    # them moi cua theo cong trinh
    @http.route("/anv/door_insert", auth='user', type='json')
    def door_insert(self, **params):
        try:
            if params['name']:
                #nếu id=0 thì tạo mới cửa , id>0(update) xóa hết data theo id tạo mới
                id = params['id']
                if id > 0:
                    #xóa nhôm
                    #self.env["anv.consdoorprofile"].search([('consdoor_id', '=', id)]).unlink()
                    a = http.request.env['anv.consdooraccessories'].search([('consdoor_id', '=', id)]).unlink()
                    #xóa kính
                    http.request.env["anv.consdoorglasses"].search([('consdoor_id', '=', id)]).unlink()
                    #xóa phụ kiện
                    http.request.env["anv.consdooraccessories"].search([('consdoor_id', '=', id)]).unlink()
                    #xóa head cửa
                    http.request.env["anv.consdoor"].search([('id', '=', id)]).unlink()

                vals = {
                    'name': params['name'],
                    'door_code': params['door_code'],
                    'quantity': params['quantity'],
                    'api_link': params['api_link'],
                    'construction_id': params['construction_id'],
                    'loaicua_id': params['loaicua_id'],
                    'amount_profile': params['amount_profile'],
                    'amount_glasses': params['amount_glasses'],
                    'amount_accessories': params['amount_accessories'],
                    'amount_labor': params['amount_labor'],
                    'margin_profit': params['margin_profit'],
                    'sequence': params['sequence'],
                    'active1': params['active1'],
                    'image_medium': params['image_medium'],
                }
                new_item = http.request.env['anv.consdoor'].sudo().create(vals)
                #insert thanh nhom
                door_profile_line = params['door_profile_line'],
                for line in door_profile_line[0]:
                    # add giá trị vào data
                    vals_profile = {
                        "consdoor_id": new_item.id,
                        "name": line.get('name'),
                        "item_code": line.get('item_code'),
                        "length": line.get('length'),
                        "profile_qty": line.get('profile_qty'),
                        "edge_cut": line.get('edge_cut'),
                        "item_price": line.get('item_price'),
                        "sequence": line.get('sequence')
                    }
                    new_profile = http.request.env['anv.consdoorprofile'].sudo().create(vals_profile)
                # insert chi tiết kính
                door_glasses_line = params['door_glasses_line'],
                for line in door_glasses_line[0]:
                    # add giá trị vào data
                    vals_glasses = {
                        "consdoor_id": new_item.id,
                        "name": line.get('name'),
                        "width": line.get('width'),
                        "height": line.get('height'),
                        "quantity": line.get('quantity'),
                        "area": line.get('area'),
                        "location": line.get('location'),
                        "item_price": line.get('item_price'),
                        "item_amount": line.get('item_amount')
                    }
                    new_glasses = http.request.env['anv.consdoorglasses'].sudo().create(vals_glasses)
                # phụ kiện cho cửa
                door_accessories_line = params['door_accessories_line'],
                for line in door_accessories_line[0]:
                    vals_accessories = {
                        "consdoor_id": new_item.id,
                        "name": line.get('name'),
                        "accessories_code": line.get('accessories_code'),
                        "unit": line.get('unit'),
                        "quantity": line.get('quantity'),
                        "price": line.get('price'),
                        "amount": line.get('amount')
                    }
                    new_accessories = http.request.env['anv.consdooraccessories'].sudo().create(vals_accessories)
                data = {'success': True, 'message': 'Tạo mới hoàn thành', 'id': new_item.id}
            return data
        except Exception as e:
            return {'success': False, "error": e, 'message': 'Tạo mới thất bại'}
        
    @http.route("/anv/calculate_door", auth='public', type='json')
    def calculate_door(self, door_id):
        door = request.env['anv.consdoor'].browse(door_id).exists()
        construction = door.construction_id
        if door:
            door.do_calculate()
            construction.compute()
            return {'success': True, 'message': 'Calculate successfully', 'door_id': door_id}
        else:
            return {'success': False, "message": 'Door not found', 'door_id': door_id}

    @http.route("/anv/test", auth='public', type='json')
    def test(self, construction_id):
        construction = request.env['anv.construction'].sudo().search([('id', '=', construction_id)])
        query = """select 
                                b.name as profile_name,
                                b.item_code,
                                sum(b.length * b.profile_qty) as total_length,
                                sum(b.total_weight) as total_weight,
                                (sum(b.length * b.profile_qty)/6000) as profile_qty
                            from anv_consdoor a
                            inner join anv_consdoorprofile b on a.id = b.consdoor_id
                            where b.active1= true and a.id = %s
                            group by b.name,b.item_code
                            order by b.item_code""" % (construction_id)

        request.env.cr.execute(query)
        myresult = request.env.cr.fetchall()

        for x in myresult:
            print(x)

        return {'message': myresult}

    @http.route("/anv/add_employee", auth='none', type='json')
    def add_employee(self, userid):
        user = request.env.user.sudo().search([('id', '=', userid)])
        #construction = request.env['anv.construction'].sudo().search([('id', '=', userid)])
        if user:
            #user.action_create_employee
            #self.ensure_one()
            request.env['hr.employee'].sudo().create(dict(
                name=user.name,
                company_id=1, **request.env['hr.employee'].sudo()._sync_user(user)))

    @http.route("/anv/construction_estimates", auth='public', type='json')
    def construction_estimates(self, construction_id):
        construction = request.env['anv.construction'].sudo().search([('id', '=', construction_id)])
        khu_vuc = ''
        if len(construction.anvaddress_id.ids) > 0:  # cấp xã
            khu_vuc = khu_vuc + construction.anvaddress_id.name
            if len(construction.anvaddress_id.parent_address.ids) > 0:  # cấp huyện
                khu_vuc = khu_vuc + ' ' + construction.anvaddress_id.parent_address.name + ' ' + construction.anvaddress_id.state_address.name
        response = {
            'ten_cong_trinh': construction.name,
            'khu_vuc': khu_vuc,
            'dia_chi': construction.address,
            'chu_dau_tu': construction.owner,
            'dien_thoai': construction.mobile,
            'ghi_chu': construction.notes,
            'profile_lines': [],
            'glasses_lines': [],
            'accessories_lines': []
        }
        # nhôm
        query = """select 
                        b.name as profile_name,
                        b.item_code,
                        sum(b.length * b.profile_qty) as total_length,
                        sum(b.total_weight) as total_weight,
                        (sum(b.length * b.profile_qty)/6000) as profile_qty
                    from anv_consdoor a
                    inner join anv_consdoorprofile b on a.id = b.consdoor_id
                    inner join anv_construction c on a.construction_id = c.id
                    where b.active1= true and c.id = %s
                    group by b.name,b.item_code
                    order by b.item_code""" % (construction_id )
        #request.env.cr.execute(query)
        #myresult = request.env.cr.fetchall()

        data_cons = []
        vals = {
            'stt': 1,
            'ten_thanh_nhom': 'Khung cửa mở quay',
            'ma_nhom': 'TN1-KMQ',
            'so_luong_cay': 2,
            'chieu_dai': 6000,
            'ty_trong': 0.83,
            'trong_luong': 9.96,
            'don_gia': 126000,
            'thanh_tien': 1254960,

        }
        data_cons.append(vals)
        response.update({'profile_lines': data_cons})
        #kinh
        query_glasses = """select 
                            a.door_code,	
                            b.name as glasses_name,	
                            b.width,
                            b.height,
                            b.quantity,
                            b.location,
                            b.area,
                            b.item_price,
                            b.item_amount
                        from anv_consdoor a
                        inner join anv_consdoorglasses b on a.id = b.consdoor_id
                        inner join anv_construction c on a.construction_id = c.id
                        where b.active1= true and c.id = %s
                        order by b.name""" % (construction_id)
        request.env.cr.execute(query_glasses)
        myresult_glasses = request.env.cr.fetchall()
        glasses_lines = []
        stt = 0
        for x in myresult_glasses:
            stt = stt + 1
            vals = {
                'stt': stt,
                'ten_cua': x[0],
                'ten_kinh': x[1],
                'rong': x[2],
                'cao': x[3],
                'so_luong': x[4],
                'vi_tri': x[5],
                'dien_tich': x[6],
                'don_gia': x[7],
                'thanh_tien': x[8],
            }
            glasses_lines.append(vals)
        response.update({'glasses_lines': glasses_lines})

        # phu kien
        query_pk = """select 
                            b.accessories_code,	
                            b.name as accessories_name,
                            b.unit,	
                            sum(b.total_quantity) as quantity,
                            sum(b.amount) as amount
                        from anv_consdoor a
                        inner join anv_consdooraccessories b on a.id = b.consdoor_id
                        inner join anv_construction c on a.construction_id = c.id
                        where b.active1= true and c.id = %s
                        group by b.accessories_code,b.name,b.unit
                        order by b.accessories_code""" % (construction_id)
        request.env.cr.execute(query_pk)
        myresult_pk = request.env.cr.fetchall()
        stt_pk = 0
        pk_lines = []
        for x in myresult_pk:
            stt_pk = stt_pk + 1
            vals = {
                'stt': stt_pk,
                'ma_phu_kien': x[0],
                'ten_phu_kien': x[1],
                'dvt': x[2],
                'so_luong': x[3],
                'don_gia': 0,
                'thanh_tien': x[4],
            }
            pk_lines.append(vals)
        response.update({'accessories_lines': pk_lines})

        data = {'success': True, 'response': response, 'message': 'ok'}
        return data

    @http.route("/anv/rpt_door_detail", auth='public', type='json')
    def rpt_door_detail(self, door_id):
        obj = request.env['anv.consdoor'].sudo().search([('id', '=', door_id)])
        loai_kinh = ''
        for line in obj.basic_settings_line_ids:
            if line.name =='Loại kính':
                loai_kinh = line.edited_value if line.edited_value else ''

        response = {
            'ten_cua': obj.name,
            'ky_hieu_cua': obj.door_code,
            'hang_nhom': obj.loaicua_id.brand_id.name,
            'he_nhom': obj.loaicua_id.doorbrand_id.name,
            'rong': obj.width,
            'cao': obj.height,
            'ho_chan_canh': 0.0,
            'loai_kinh': loai_kinh,
            'so_bo': obj.quantity,
            'dien_tich': obj.total_square_metter,
            'don_gia_nhom': obj.price_alu,
            'sl_kg_nhom': obj.total_kg_profile,
            'thanh_tien_nhom': obj.amount_profile,
            'don_gia_kinh': obj.price_glass,
            'sl_m_kinh': obj.total_m2_glasses,
            'thanh_tien_kinh': obj.amount_glasses,
            'don_gia_pk': obj.amount_accessories,
            'sl_pk': obj.quantity,
            'thanh_tien_pk': obj.total_amount_accessories,
            'don_gia_nc': obj.amount_labor,
            'thanh_tien_nc': (obj.amount_labor * obj.total_square_metter),
            'tong_tien': obj.amount_profile + obj.amount_glasses + obj.total_amount_accessories + (obj.amount_labor * obj.total_square_metter),
            'anh': obj.image_medium,
            'profile_lines': [],
            'glasses_lines': [],
            'accessories_lines': []
        }
        #chi tiết nhôm
        data_profile = []
        for x in obj.door_profile_line:
            vals = {
                'ten_thanh_nhom': x.name,
                'vi_tri_thanh': x.location,
                'ky_hieu': x.item_code,
                'goc_cat': x.edge_cut,
                'so_luong': x.profile_qty,
                'kich_thuoc': x.length,
            }
            data_profile.append(vals)
        response.update({'profile_lines': data_profile})

        # chi tiết kinh
        data_glasses = []
        for x in obj.door_glasses_line:
            vals = {
                'ten_kinh': x.name,
                'rong_kinh': x.width,
                'cao_kinh': x.height,
                'so_luong': x.quantity,
                'dien_tich': x.area,
                'vi_tri': x.location,
            }
            data_glasses.append(vals)
        response.update({'glasses_lines': data_glasses})

        # chi tiết phụ kiện
        data_accessories = []
        for x in obj.door_accessories_line:
            vals = {
                'ten_phu_kien': x.name,
                'ky_hieu': x.accessories_code,
                'don_vi_tinh': x.unit,
                'so_luong': x.total_quantity,
            }
            data_accessories.append(vals)
        response.update({'accessories_lines': data_accessories})


        data = {'success': True, 'response': response, 'message': 'ok'}
        return data

    #bao gia cong trinh
    @http.route("/anv/rpt_inquiry", auth='public', type='json')
    def rpt_door_inquiry(self, construction_id):
        obj = request.env['anv.construction'].sudo().search([('id', '=', construction_id)])
        khu_vuc = ''
        if len(obj.anvaddress_id.ids) > 0:#cấp xã
            khu_vuc = khu_vuc + obj.anvaddress_id.name
            if len(obj.anvaddress_id.parent_address.ids) > 0: #cấp huyện
                khu_vuc = khu_vuc + ' ' + obj.anvaddress_id.parent_address.name + ' ' + obj.anvaddress_id.state_address.name
        response = {
            'ten_cong_trinh': obj.name,
            'khu_vuc': khu_vuc,
            'dia_chi': obj.address,
            'dien_thoai': obj.mobile,
            'chu_dau_tu': obj.owner,
            'ghi_chu': obj.notes,
            'dai_ly': obj.create_uid.name,
            'door_lines': [],
        }
        # chi tiết nhôm
        data_door = []
        stt = 0
        for x in obj.door_lines:
            stt = stt + 1
            dien_tich = (x.width * x.height)/1000000
            tong_dien_tich = x.quantity * dien_tich
            don_gia_pk_m2 = (x.total_amount_accessories * x.margin_profit)/ tong_dien_tich
            vals = {
                'stt': stt,
                'ky_hieu': x.door_code,
                'dien_giai': x.name,
                'rong': x.width,
                'cao': x.height,
                'dien_tich': dien_tich,
                'sl_bo': x.quantity,
                'tong_dien_tich': tong_dien_tich,
                'dongia_m2': x.price_per_square,
                'dongia_pk_bo': don_gia_pk_m2,
                'image': x.image_medium,
            }
            data_door.append(vals)
        response.update({'door_lines': data_door})

        data = {'success': True, 'response': response, 'message': 'ok'}
        return data

    @http.route("/anv/update_construction", auth='public', type='json')
    def update_construction(self,construction_id, labor_per_metter, margin_profit):
        construction = request.env['anv.construction'].browse(construction_id).exists()
        if construction:
            construction.update_construction(labor_per_metter, margin_profit)
            return {'success': True, 'message': 'update successfully', 'construction_id': construction_id}
        else:
            return {'success': False, "message": 'construction not found', 'construction_id': construction_id}