from odoo import models, fields, api, _

class ResCompany(models.Model):
    _inherit = 'res.company'

    calculator_service_url = fields.Char(string='Calculator Service URL')
    api_key = fields.Char(string='API Key')
