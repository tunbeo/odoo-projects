# -*- coding: utf-8 -*-
from odoo import fields, models, _


class Partner(models.Model):
    _inherit = 'mail.message'

    like_by_ids = fields.Many2many('res.partner', 'message_like_ids', string="Like by ids")