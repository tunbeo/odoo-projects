# -*- coding: utf-8 -*-
from odoo import fields, models, _


class User(models.Model):
    _inherit = 'res.users'

    favorites_doorbrand_ids = fields.Many2one('adg.doorbrand', string="Hãng / tài liệu yêu thích", default=[])
    farorites_course_ids = fields.Many2one('slide.channel', string="Khoá học yêu thích", default=[])
    favorites_catalog = fields.Many2one('anv.catalog', string="Catalog của tôi", default=[])

    anv_gender = fields.Boolean(string="ANV Gender")
    anv_birthday = fields.Char(string="ANV Birthday")
    anv_contact_address = fields.Char(string="ANV Contact Address")
