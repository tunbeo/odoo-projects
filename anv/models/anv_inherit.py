from odoo import models, fields, _, api
from odoo.exceptions import ValidationError

class AnvInherit(models.Model):
    _inherit = 'res.users'
    # instructors
    anvmobile = fields.Char(string="mobile")
    anvotp = fields.Char(string="otp")
    anvdevice_name = fields.Char(string="device_name")
    anvaddress_id = fields.Many2one('address.book', string="Khu vực")