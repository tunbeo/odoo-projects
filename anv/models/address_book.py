from odoo import models, fields, _, api
from odoo.exceptions import ValidationError

class AddressBook(models.Model):
    _name = 'address.book'
    _description = 'Address book'

    name = fields.Char(string='Address Name', required=True)
    address_level = fields.Selection([
                            ('level_1__ward', 'Ward'),
                            ('level_2__district', 'District'),
                            ('level_3__city', 'City'),
                        ], required=True)
    address_code = fields.Char(string="Address Code")
    parent_address = fields.Many2one('address.book', string='Parent Address')
    state_address = fields.Many2one('res.country.state', string='State Address')
    address_full = fields.Char(string='Address full', compute='_compute_address_full')

    @api.depends('parent_address', 'name', 'address_full')
    def _compute_address_full(self):
        for r in self:
            address_full = r.name
            if r.parent_address.name:
                 address_full = address_full + ', ' + r.parent_address.name + ', ' + r.state_address.name

            r.address_full = address_full
    @api.constrains('parent_address')
    def _check_parent_address(self):
        for r in self:
            parent_address = r.parent_address
            parent_address_level = None
            parent_address_name = None
            for p in parent_address:
                parent_address_level = p.address_level
                parent_address_name = p.name
            if r.address_level >= parent_address_level:
                raise ValidationError(_("The address %s can not be child of %s",
                                        r.name,
                                        parent_address_name))
