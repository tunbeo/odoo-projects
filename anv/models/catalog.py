# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from odoo import models, fields, api, exceptions, _


class Catalog(models.Model):
    _name = 'anv.catalog'
    _description = 'Catalog'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"

    name = fields.Char(string="Tên catalog", required=True)  # model
    url = fields.Char(string="Link catalog", required=True)  # model

    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active", help="True: có hiệu lực,False:không có hiệu lực")


class Thumbnail(models.Model):
    _name = 'anv.thumbnail'
    _description = 'Thumbnail'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"

    name = fields.Char(string="Tên thumbnail", required=True)  # model
    image_medium = fields.Binary(
        "Image", attachment=True,
        help="Medium-sized image of the product. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved, "
             "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")

    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active", help="True: có hiệu lực,False:không có hiệu lực")