# -*- coding: utf-8 -*-
from odoo import fields, models, _


class Partner(models.Model):
    _inherit = 'res.partner'

    favorites_doorbrand_ids1 = fields.Many2many('adg.doorbrand', 'favorites_by_ids', string="Hãng / tài liệu yêu thích")

    message_like_ids = fields.Many2many('mail.message', 'like_by_ids', string="Like messages")

