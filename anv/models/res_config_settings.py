from odoo import models, fields

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    calculator_service_url = fields.Char(string='Calculator Service URL', related='company_id.calculator_service_url', readonly=False)
    api_key = fields.Char(string='API Key', related='company_id.api_key', readonly=False)
