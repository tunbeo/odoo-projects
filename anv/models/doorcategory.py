# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from odoo import models, fields, api, exceptions, _


class door(models.Model):
    _name = 'anv.doorcategory'
    _description = 'loại cửa'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Loại cửa", required=True)  # model
    category_code = fields.Char(string="Mã cửa")
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=1)
    active1 = fields.Boolean(default=True, string="active", help="True: có hiệu lực,False:không có hiệu lực")
    image = fields.Binary(
        "Image", attachment=True,
        help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True,
        help="Medium-sized image of the product. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved, "
             "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    image_small = fields.Binary(
        "Small-sized image", attachment=True,
        help="Small-sized image of the product. It is automatically "
             "resized as a 64x64px image, with aspect ratio preserved. "
             "Use this field anywhere a small image is required.")