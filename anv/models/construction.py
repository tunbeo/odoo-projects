# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from odoo import models, fields, api, exceptions, _
import random
_logger = logging.getLogger(__name__)


class door(models.Model):
    _name = 'anv.construction'
    _description = 'Công trình cửa topal'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"

    name = fields.Char(string="Tên công trình", required=True)  # model
    address = fields.Char(string="Địa chỉ")
    owner = fields.Char(string="Khách hàng")
    mobile = fields.Char(string="Điện thoại khách hàng")
    notes = fields.Char(string="Ghi chú", track_visibility='onchange')
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active", help="True: có hiệu lực,False:không có hiệu lực")
    door_lines = fields.One2many('anv.consdoor', 'construction_id', string='Danh sách cửa', track_visibility='always')
    anvaddress_id = fields.Many2one('address.book', string="Khu vực")
    labor_per_metter = fields.Float(string="Gía nhân công / m2", default=200000.00, store=True)
    margin_profit = fields.Float(string="Tỉ suất lợi nhuận công trình", default=1.1, store=True)

    amount = fields.Float(string="Tổng tiền", help="Tổng giá thành tất cả cửa trong công trình", default=0.00, compute='_compute_amount_total', store=True)


    total_square_metter = fields.Float(string="Tổng diện tích công trình", default=0.00, compute='_compute_amount_total', store=True)

    price_per_metter = fields.Float(string="Đơn giá toàn công trình", default=0.00, compute='_compute_amount_total', store=True)

    total_amount_profile = fields.Float(string="Tổng tiền nhôm toàn công trình", default=0.00, compute='_compute_amount_total', store=True)
    total_kg_profile = fields.Float(string="Tổng kg nhôm toàn công trình", default=0.00,
                                        compute='_compute_amount_total', store=True)
    total_amount_glasses = fields.Float(string="Tổng tiền kính toàn công trình", default=0.00,
                                        compute='_compute_amount_total', store=True)
    total_m2_glasses = fields.Float(string="Tổng m2 kính toàn công trình", default=0.00,
                                            compute='_compute_amount_total', store=True)
    total_amount_accessories = fields.Float(string="Tổng tiền phụ kiện toàn công trình", default=0.00,
                                        compute='_compute_amount_total', store=True)
    total_number_of_accessories = fields.Integer(string="Tổng số phụ kiện toàn công trình", default=0,
                                        compute='_compute_amount_total', store=True)
    name_image = fields.Char(string="Tên ảnh công trình")

    #anvaddress_full = fields.Char(string='Address full', compute='_compute_anvaddress_full', store=True)
    anvaddress_full = fields.Char(string='Address full', related='anvaddress_id.address_full', store=True)
    anvaddress_full2 = fields.Char(string='Address full 2', compute='_compute_anvaddress_full', store=True)

    @api.model
    def create(self, values):
        random_name = random.randint(1, 20)
        _name_image = str(random_name) + '.jpg'
        values.update({'name_image': _name_image})
        res = super(door, self).create(values)
        return res

    def update_construction(self, labor_per_metter, margin_profit):
        try:
            update_data = {'labor_per_metter': labor_per_metter,
                           'margin_profit': margin_profit
                           }
            for a in self.door_lines:
                a.amount_labor = labor_per_metter
                a.margin_profit = margin_profit
            self.update(update_data)
            self._compute_amount_total()
        except Exception as e:
            _logger.error("lỗi update lợi nhuận công trình. Detail error: %s" % e)
            raise Exception(_("lỗi update lợi nhuận công trình."))

    def compute(self):
        self._compute_amount_total()

    @api.depends("door_lines")
    def _compute_amount_total(self):
        amount_total = 0
        total_square = 0
        price_per_met = 0
        total_kg_profile = 0
        total_amount_profile = 0
        total_amount_glasses = 0
        total_m2_glasses = 0
        total_amount_accessories = 0
        total_number_of_accessories = 0

        lines = self.door_lines
        for a in lines:
            amount_total = amount_total + a.amount_total # tổng tiền
            total_square = total_square + a.total_square_metter # tổng m2 cửa công trình
            if total_square > 0:
                price_per_met = amount_total / total_square # đơn giá công trình - ngu vl
            total_kg_profile = total_kg_profile + a.total_kg_profile
            total_amount_profile = total_amount_profile + a.price_alu
            total_amount_glasses = total_amount_glasses + a.price_glass
            total_m2_glasses = total_m2_glasses + a.total_m2_glasses
            total_amount_accessories = total_amount_accessories + a.total_amount_accessories
            #total_number_of_accessories = total_number_of_accessories + a.number_of_accessories


        self.amount = amount_total
        self.total_square_metter = total_square
        self.price_per_metter = price_per_met
        self.total_kg_profile = total_kg_profile
        self.total_amount_profile = total_amount_profile
        self.total_amount_glasses = total_amount_glasses
        self.total_m2_glasses = total_m2_glasses
        self.total_amount_accessories = total_amount_accessories
        self.total_number_of_accessories = total_number_of_accessories

    @api.depends("address")
    def _compute_anvaddress_full(self):
        for r in self:
            _address_full = ''
            if r.address:
                 _address_full = r.address
            if r.anvaddress_full:
                _address_full = _address_full + ', ' + r.anvaddress_full.replace('Quận', '')
                _address_full = _address_full.replace('Huyện', '')
            r.anvaddress_full2 = _address_full
            #test