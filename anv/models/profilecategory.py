# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from odoo import models, fields, api, exceptions, _


class profilecategory(models.Model):
    _name = 'anv.profilecategory'
    _description = 'Hãng nhôm'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Hãng nhôm", required=True)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=1)
    active1 = fields.Boolean(default=True, string="active", help="True: có hiệu lực,False:không có hiệu lực")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True,
        help="Medium-sized image of the product. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved, "
             "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    _sql_constraints = [
        ('name_unique',
         'UNIQUE(name)',
         "Tên loại hệ cửa nhôm này đã tồn tại"),
    ]