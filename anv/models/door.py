# -*- coding: utf-8 -*-
import requests
import urllib.parse
import logging
import os
from datetime import timedelta
from odoo import models, fields, api, exceptions, _

_logger = logging.getLogger(__name__)

class ConsDoor(models.Model):
    _name = 'anv.consdoor'
    _description = 'Cửa theo công trình'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'loaicua_id,sequence,id'

    name = fields.Char(string="Tên cửa", required=True)
    door_template_id = fields.Char(string='Door ID', compute='_compute_door_template_id', store=True)
    door_code = fields.Char(string="Ký hiệu", required=True)
    quantity = fields.Integer(string="Số lượng", default=1)
    profile_type = fields.Char(string="Hệ nhôm")
    brand_name = fields.Char(string="Hãng nhôm", compute='_compute_door_brand', store=True)
    api_link = fields.Char(string="Link API")
    profile_id = fields.Many2one('anv.profilecategory',string="Ngành nhôm", store=True)
    loaicua_id = fields.Many2one('adg.door', string="Loại cửa", store=True)
    construction_id = fields.Many2one('anv.construction', string="Công trình", store=True)
    door_id = fields.Many2one('anv.doorcategory', string="Loại cửa", store=True)

    amount_profile = fields.Float(string="Tiền nhôm", default=0.00, store=True)
    amount_accessories = fields.Float(string="Tiền phụ kiện của 1 bộ", default=0.00, store=True)
    total_amount_accessories = fields.Float(string="Tổng tiền phụ kiện", compute="_compute_total_amount_accessories",
                                            store=True)
    amount_glasses = fields.Float(string="Tiền kính", default=0.00, store=True)
    amount_labor = fields.Float(string="Tiền nhân công / m2", default=200000.00, store=True) # related='construction_id.labor_per_metter'
    total_amount_labor = fields.Float(string="Tổng tiền nhân công", compute="_compute_total_amount_labor", store=True)
    amount_cost = fields.Float(string="Giá cost", help="Giá cost = Nhôm + Kính + Phụ kiện tự điền + Nhân công * diện tích cửa ",
                               digits=(12, 2), compute='_compute_amount_cost', store=True, track_visibility='onchange')
    margin_profit = fields.Float(string="Margin lợi nhuận",digits=(12, 2), default=1.1, store=True) # related='construction_id.margin_profit'
    amount_total = fields.Float(string="Giá bán", help="Giá cost * Margin lợi nhuân", compute='_compute_amount_total', store=True)
    notes = fields.Char(string="Ghi chú")
    door_profile_line = fields.One2many('anv.consdoorprofile', 'consdoor_id', string='Kích thước cắt nhôm',
                                        track_visibility='always', copy=True)
    door_glasses_line = fields.One2many('anv.consdoorglasses', 'consdoor_id', string='Kích thước cắt kính',
                                        track_visibility='always', copy=True)
    door_accessories_line = fields.One2many('anv.consdooraccessories', 'consdoor_id', string='Chi tiết phụ kiện',
                                        track_visibility='always', copy=True)
    door_price_line = fields.One2many('anv.consdoorprice', 'consdoor_id', string='Chi tiết phụ kiện',
                                            track_visibility='always', copy=True)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active", help="True: có hiệu lực,False:không có hiệu lực")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True,
        help="Medium-sized image of the product. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved, "
             "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    basic_settings_line_ids = fields.One2many('anv.consdoor.base.settings', 'anv_consdoor_id', string='Basic Settings')
    advanced_settings_line_ids = fields.One2many('anv.consdoor.advanced.settings', 'anv_consdoor_id', string='Advanced Settings')

    square_metter = fields.Float(string="Diện tích đơn (m2)", compute='_compute_square_metter', digits=(12, 2), default=0.00, store=True)
    total_square_metter = fields.Float(string="Tổng diện tích", compute='_compute_square_metter', digits=(12, 2), default=0.00, store=True)
    total_m2_glasses = fields.Float(string="Tổng diện tích kính", digits=(12, 2), default=0.00, store=True)
    total_kg_profile = fields.Float(string="Tổng kg nhôm", digits=(12, 2), default=0.00, store=True)
    width = fields.Float(string="Rộng",digits=(12, 2), default=0.00, store=True)
    height = fields.Float(string="Cao",digits=(12, 2), default=0.00, store=True)

    price_alu = fields.Float(string="Giá nhôm", digits=(12, 2), default=0.00, store=True)
    price_glass = fields.Float(string="Giá kính", digits=(12, 2), default=0.00, store=True)

    price_per_square = fields.Float(string="Đơn giá (m2)", compute='_compute_price_per_square_cost', help="Đơn giá bán = Margin Lợi nhuận * ( Nhôm + Kính + Nhân công / m2) => record.price_per_square = ((record.amount_cost - record.amount_accessories) / record.quantity) / record.square_metter", digits=(12, 2), default=0.00, store=True)
    number_of_accessories = fields.Integer(string="Số lượng phụ kiện", default=0, compute='_compute_number_of_accessories')

    @api.depends('amount_labor', 'total_square_metter')
    def _compute_total_amount_labor(self):
        for r in self:
            r.total_amount_labor = r.amount_labor * r.total_square_metter

    @api.depends('quantity','amount_accessories')
    def _compute_total_amount_accessories(self):
        for r in self:
            r.total_amount_accessories = r.quantity * r.amount_accessories

    @api.depends('door_accessories_line')
    def _compute_number_of_accessories(self):
        for r in self:
            if self.door_accessories_line:
                total_accessories = 0
                for a in self.door_accessories_line:
                    total_accessories = total_accessories + a.quantity
                r.number_of_accessories = total_accessories

    @api.depends('loaicua_id')
    def _compute_door_template_id(self):
        for r in self:
            r.door_template_id = ''
            if r.loaicua_id and r.loaicua_id.excelFilePath and '.' in r.loaicua_id.excelFilePath:
                r.door_template_id = os.path.splitext(r.loaicua_id.excelFilePath)[0]
    
    @api.depends("amount_profile", "amount_accessories", "amount_glasses", "amount_labor", "total_square_metter")
    def _compute_amount_cost(self):
        for record in self:
            record.amount_cost = (record.amount_profile + record.amount_glasses + record.total_amount_accessories + record.amount_labor * record.total_square_metter)

            # Cost = Nhom + Kinh + Phu kien + (Nhan cong * m2)

    @api.depends("amount_cost", "square_metter", "margin_profit", "quantity", "total_amount_accessories")
    def _compute_price_per_square_cost(self):
        for r in self:
            if r.square_metter != 0 and r.quantity != 0:
                #r.price_per_square = ((r.amount_cost - r.amount_accessories) / r.quantity) / r.square_metter
                #r.price_per_square = r.amount_total / r.square_metter / r.quantity
                r.price_per_square = (r.amount_cost - r.total_amount_accessories) * r.margin_profit / r.square_metter / r.quantity


    @api.depends("amount_cost", "margin_profit", "quantity")
    def _compute_amount_total(self):
        for record in self:
            record.amount_total = (record.amount_cost * record.margin_profit)

    @api.depends("width", "height", "quantity")
    def _compute_square_metter(self):
        for record in self:
            record.square_metter = record.width * record.height / 1000000.00
            record.total_square_metter = record.square_metter * record.quantity

    @api.onchange('loaicua_id')
    def _onchange_loaicua_id(self):
        self.basic_settings_line_ids.unlink()
        self.advanced_settings_line_ids.unlink()
        self._set_api_link()
        for record in self:
            record.brand_name = record.loaicua_id.brand_id.name

    @api.depends('loaicua_id')
    def _compute_door_brand(self):
        for record in self:
            record.brand_name = record.loaicua_id.brand_id.name

    @api.onchange('basic_settings_line_ids', 'advanced_settings_line_ids')
    def _onchange_settings(self):
        self._set_param_for_api_link()
        
    def _set_param_for_api_link(self):
        self.ensure_one()
        current_link = self.api_link
        
        params = {}
        for basic_setting in self.basic_settings_line_ids:
            if basic_setting.edited_value != basic_setting.value:
                params[basic_setting.cell] = basic_setting.edited_value
        
        for advanced_setting in self.advanced_settings_line_ids:
            if advanced_setting.edited_value != advanced_setting.value:
                params[advanced_setting.cell] = advanced_setting.edited_value
        
        url_parse = urllib.parse.urlparse(current_link)
        query = url_parse.query
        url_dict = dict(urllib.parse.parse_qsl(query))
            
        if params:
            url_dict.update(params)
        else:
            for key in url_dict.keys():
                basic_setting = self.basic_settings_line_ids.filtered(lambda setting: setting.cell == key)
                advanced_setting = self.advanced_settings_line_ids.filtered(lambda setting: setting.cell == key)
                if basic_setting:
                    url_dict[key] = basic_setting.edited_value
                elif advanced_setting:
                    url_dict[key] = advanced_setting.edited_value
            
        url_new_query = urllib.parse.urlencode(url_dict)
        url_parse = url_parse._replace(query=url_new_query)
        url = urllib.parse.urlunparse(url_parse)
    
        self.api_link = url
            
    def _set_api_link(self):
        self.ensure_one()
        base_url = self.env.company.calculator_service_url
        api_key = self.env.company.api_key
        key_query = '?key=%s' % api_key
        
        url_part1 = urllib.parse.urljoin(base_url, self.door_template_id)
        url = urllib.parse.urljoin(url_part1, key_query)
        
        self.api_link = url
    
    def action_reset_calculate(self):
        self.ensure_one()
        self.basic_settings_line_ids.unlink()
        self.advanced_settings_line_ids.unlink()
        self._set_api_link()
        
        self.do_calculate()
    
    def action_calculate(self):
        self.ensure_one()
        self.do_calculate()
        
    def do_calculate(self):
        try:
            door_res = requests.get(self.api_link)
            door_res_data = door_res.json()
            res_message = door_res_data.get('message', False)
            if res_message and res_message == 'OK':
                
                door_data = door_res_data.get('doorTables')
                basic_settings_data = door_res_data.get('baseSetting')
                advanced_settings_data = door_res_data.get('advanceSetting')
                door_img_data = door_res_data.get('base64Img')
                #square = door_res_data.get('totalSquareMetter')

                width = 0.00
                height = 0.00
                price_alu = 0.00
                price_glass = 0.00

                for a in basic_settings_data:
                    if 'Giá nhôm' in a.get('name'):  # == 'Giá nhôm (VNĐ/Kg)':
                        price_alu = self._convert_string_to_numer_format(a.get('value')) if a.get('value') else 0.00
                    elif 'Giá kính' in a.get('name'):  # == 'Giá kính':
                        price_glass = self._convert_string_to_numer_format(a.get('value')) if a.get('value') else 0.00
                    elif 'Rộng Cửa (B)' in a.get('name'):  # == 'Rộng Cửa':
                        width = self._convert_string_to_numer_format(a.get('value')) if a.get('value') else 0.00
                    elif 'Cao Cửa (H)' in a.get('name'):  # == 'Cao Cửa':
                        height = self._convert_string_to_numer_format(a.get('value')) if a.get('value') else 0.00

                basic_settings_line_update = []
                advanced_settings_line_update = [] 
                profile_line_update = []
                glass_line_update = []
                accessory_line_update = []
                pricelist_line_update = []
                
                if not self.basic_settings_line_ids:
                    basic_settings_line_update = self._build_basic_settings_data(basic_settings_data)
                
                if not self.advanced_settings_line_ids:
                    advanced_settings_line_update = self._build_advanced_settings_data(advanced_settings_data)
                    
                if door_data:
                    profiles_data = door_data.get('Alu')
                    glasses_data = door_data.get('Glass')
                    accessories_data = door_data.get('Accessories')
                    pricelists_data = door_data.get('PriceList')
                    
                    profile_line_update = self._build_profile_data(profiles_data, price_alu)
                    glass_line_update = self._build_glass_data(glasses_data, price_glass)
                    accessory_line_update = self._build_accessory_data(accessories_data)
                    pricelist_line_update = self._build_pricelist_data(pricelists_data)

                    self._get_amount_pricelists_data(pricelists_data)
                    self._get_basic_settings_data(basic_settings_data)
                
                self.door_profile_line.unlink()
                self.door_glasses_line.unlink()
                self.door_accessories_line.unlink()
                self.door_price_line.unlink()
                update_data = {'door_profile_line': profile_line_update,
                             'door_glasses_line': glass_line_update,
                             'door_accessories_line': accessory_line_update,
                             'door_price_line': pricelist_line_update,
                             'image_medium':door_img_data,
                             #'square_metter':square,
                             'width':width,
                             'height':height,
                             'price_alu':price_alu,
                             'price_glass':price_glass
                             }

                if basic_settings_line_update:
                    update_data.update({'basic_settings_line_ids': basic_settings_line_update})
                    
                if advanced_settings_line_update:
                    update_data.update({'advanced_settings_line_ids': advanced_settings_line_update})
                self.update(update_data)
            else:
                _logger.error("Error when calling to service API. Detail error: %s" % res_message)
                raise Exception(_("Error when calling to service API."))
        except Exception as e:
            _logger.error("Error when calling to service API. Detail error: %s" % e)
            raise Exception(_("Error when calling to service API."))

    def _build_profile_data(self, profiles_data, price_alu):
        line_to_update = []
        for profile in profiles_data:
            total_weight = self._convert_string_to_numer_format(profile.get('total_weight')) if profile.get('total_weight') else 0.00
            line_to_update.append((0, 0, {'name': profile.get('name'),
                                          'item_code': profile.get('code'),
                                          'location': profile.get('location'),
                                          'edge_cut': profile.get('angle'),
                                          'profile_qty': self._convert_string_to_numer_format(profile.get('quantity')) if profile.get('quantity') else 0,
                                          'length': self._convert_string_to_numer_format(profile.get('length')) if profile.get('length') else 0.00,
                                          'weight_per_metter': self._convert_string_to_numer_format(profile.get('weight')) if 
                                                profile.get('weight') else 0.00,
                                          'total_weight': total_weight,
                                          'item_price': price_alu,
                                          'item_amount': float(price_alu) * float(total_weight)})),

        return line_to_update
    
    def _build_glass_data(self, glasses_data, price_glass):
        line_to_update = []
        for glass in glasses_data:
            area = self._convert_string_to_numer_format(glass.get('s')) if glass.get('s') else 0.00
            line_to_update.append((0, 0, {'name': glass.get('name'),
                                          'width': self._convert_string_to_numer_format(glass.get('width')) if glass.get('width') else 0.00,
                                          'height': self._convert_string_to_numer_format(glass.get('height')) if glass.get('height') else 0.00,
                                          'quantity': self._convert_string_to_numer_format(glass.get('quantity')) if glass.get('quantity') else 0.00,
                                          'area': area,
                                          'location': glass.get('location'),
                                          'item_price': price_glass,
                                          'item_amount': float(area) * float(price_glass)}))
        return line_to_update
    
    def _build_accessory_data(self, accessories_data):
        line_to_update = []
        for accessory in accessories_data:
            line_to_update.append((0, 0, {'name': accessory.get('name'),
                                          'accessories_code': accessory.get('code'),
                                          'unit': accessory.get('unit'),
                                          'total_quantity': self._convert_string_to_numer_format(accessory.get('quantity')) if accessory.get('quantity') else 0.00}))
        return line_to_update
    
    def _build_pricelist_data(self, pricelists_data):
        line_to_update = []
        for pricelist in pricelists_data:
            line_to_update.append((0, 0, {'name': pricelist.get('name'),
                                          'unit': pricelist.get('unit') if pricelist.get('unit') else 'N/A',
                                          'quantity': self._convert_string_to_numer_format(pricelist.get('quantity')) if pricelist.get('quantity') 
                                                else 0.00,
                                          'price': self._convert_string_to_numer_format(pricelist.get('price')) if pricelist.get('price') else 0.00,
                                          'amount': self._convert_string_to_numer_format(pricelist.get('sum')) if pricelist.get('sum') else 0.00}))
        return line_to_update
    
    def _build_basic_settings_data(self, basic_settings_data):
        line_to_update = []
        for setting in basic_settings_data:
            line_to_update.append((0, 0, {'name': setting.get('name'),
                                          'cell': setting.get('cell'),
                                          'value': setting.get('value'),
                                          'edited_value': setting.get('value')}))
        return line_to_update
    
    def _build_advanced_settings_data(self, advanced_settings_data):
        line_to_update = []
        for setting in advanced_settings_data:
            line_to_update.append((0, 0, {'name': setting.get('name'),
                                          'cell': setting.get('cell'),
                                          'value': setting.get('value'),
                                          'edited_value': setting.get('value')}))
        return line_to_update

    def _get_amount_pricelists_data(self, pricelists_data):
        for setting in pricelists_data:
            if setting.get('name') == 'Nhôm (VNĐ)':
                self.amount_profile = self._convert_string_to_numer_format(setting.get('sum')) if setting.get('sum') else 0.00
                self.total_kg_profile = self._convert_string_to_numer_format(setting.get('quantity')) if setting.get('quantity') else 0.00
            elif setting.get('name') == 'Kính (VNĐ)':
                self.amount_glasses = self._convert_string_to_numer_format(setting.get('sum')) if setting.get('sum') else 0.00
                self.total_m2_glasses = self._convert_string_to_numer_format(setting.get('quantity')) if setting.get('quantity') else 0.00

    def _get_basic_settings_data(self, basic_settings_data):
        for setting in basic_settings_data:
            if 'Số bộ' in setting.get('name'):  # == ' lấy giá trị Số lượng bộ cửa':
                self.quantity = self._convert_string_to_numer_format(setting.get('value')) if setting.get('value') else 0


    @api.model
    def _convert_string_to_numer_format(self, float_string):
        if ',' in float_string:
            return float_string.replace(',', '')
        else:
            return float_string

#Chi tiết vật tư thanh nhôm theo cửa
class ConsDoorProfile(models.Model):
    _name = 'anv.consdoorprofile'
    _description = 'Bảng chi tiết nhôm theo cửa'
    _order = 'sequence,id'

    name = fields.Char(string="Tên thanh nhôm", required=True)
    item_code = fields.Char(string="Ký hiệu", required=True)
    consdoor_id = fields.Many2one('anv.consdoor', ondelete='cascade', string="Cửa",required=True,  change_default=True)
    length = fields.Float(string="Kích thước(mm)", digits=(12, 00), default=0.00)
    profile_qty = fields.Integer(string="Số lượng", default=0)
    edge_cut = fields.Char(string="Góc cắt", store=True)
    weight_per_metter = fields.Float(string="Trọng lượng", digits=(12, 2), default=0.00)
    total_weight = fields.Float(string="Tổng trọng lượng", digits=(12, 2), default=0.00)

    location = fields.Char(string="Vị trí")
    item_price = fields.Float(string="Đơn giá", digits=(12, 2), default=0.00)
    item_amount = fields.Float(string="Thành tiền", digits=(12, 2), default=0.00)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active")


#Chi tiết kính theo cửa
class ConsDoorGlasses(models.Model):
    _name = 'anv.consdoorglasses'
    _description = 'Bảng chi tiết kính theo cửa'
    _order = 'sequence,id'

    name = fields.Char(string="Tên kính", required=True)
    consdoor_id = fields.Many2one('anv.consdoor', ondelete='cascade', string="Cửa",required=True,  change_default=True)
    width = fields.Float(string="Rộng(mm)", digits=(12, 00), default=0.00)
    height = fields.Float(string="Cao(mm)", digits=(12, 00), default=0.00)
    quantity = fields.Float(string="Số lượng", digits=(12, 2), default=0.00)
    area = fields.Float(string="Diện tích", digits=(12, 2), default=0.00)
    location = fields.Char(string="Vị trí")
    item_price = fields.Float(string="Đơn giá", digits=(12, 2), default=0.00)
    item_amount = fields.Float(string="Thành tiền", digits=(12, 2), default=0.00)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active")


#Chi tiết phụ kiện
class ConsDoorAccessories(models.Model):
    _name = 'anv.consdooraccessories'
    _description = 'Bảng chi tiết phụ kiện theo cửa'
    _order = 'sequence,id'

    name = fields.Char(string="Tên", required=True)
    accessories_code = fields.Char(string="Mã")
    consdoor_id = fields.Many2one('anv.consdoor', ondelete='cascade', string="Cửa",required=True,  change_default=True)
    unit = fields.Char(string="ĐVT")
    quantity = fields.Float(string="Số lượng / bộ", digits=(12, 2),compute="_compute_total_amount_accessories", store=True , default=0.00)
    number_of_sets = fields.Integer(string="Số bộ", related="consdoor_id.quantity", store=True)
    total_quantity = fields.Float(string="Tổng SL", digits=(12, 2), default=0.00, store=True)
    price = fields.Float(string="Đơn giá", digits=(12, 2), default=0.00)
    amount = fields.Float(string="Thành tiền", digits=(12, 2), default=0.00)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active")

    @api.depends('total_quantity', 'number_of_sets')
    def _compute_total_amount_accessories(self):
        for r in self:
            r.quantity = r.total_quantity / r.number_of_sets


#Chi tiết PriceList theo cửa
class ConsDoorPriceList(models.Model):
    _name = 'anv.consdoorprice'
    _description = 'Bảng chi tiết các loại giá theo cửa'
    _order = 'sequence,id'

    name = fields.Char(string="Tên", required=True)
    consdoor_id = fields.Many2one('anv.consdoor', ondelete='cascade', string="Cửa", required=True,  change_default=True)
    unit = fields.Char(string="ĐVT")
    quantity = fields.Float(string="Số lượng", digits=(12, 2), default=0.00)
    price = fields.Float(string="Đơn giá", digits=(12, 2), default=0.00)
    amount = fields.Float(string="Thành tiền", digits=(12, 2), default=0.00)
    sequence = fields.Integer(string='Thứ tự sắp xếp', copy=False, default=100)
    active1 = fields.Boolean(default=True, string="Active")
    
class ConsDoorBaseSettings(models.Model):
    _name = 'anv.consdoor.base.settings'
    _description = 'Construction Door Base Settings'
    
    name = fields.Char(string='Name', readonly=True)
    cell = fields.Char(string='Cell')
    value = fields.Char(string='Original Value')
    edited_value = fields.Char(string='Value')
    anv_consdoor_id = fields.Many2one('anv.consdoor', string='Door')

class ConsDoorAdvancedSettings(models.Model):
    _name = 'anv.consdoor.advanced.settings'
    _description = 'Construction Door Advanced Settings'
    
    name = fields.Char(string='Name', readonly=True)
    cell = fields.Char(string='Cell')
    value = fields.Char(string='Original Value')
    edited_value = fields.Char(string='Value')
    anv_consdoor_id = fields.Many2one('anv.consdoor', string='Door')
