odoo.define("tmg_website.ProductCard", function(require) {
    const { Component } = owl;
    const { xml } = owl.tags;

    class ProductCard extends Component {
        static template = xml`
        <div class="col-4 col-sm-6 col-md-4 col-lg-3 col-xl-2" style="border: 0.5px solid; justify-content: center; padding: 5px;">
            <div class="card-inverse text-center box-shadow-0">
                <div>
                    <a t-att-href="props.product.link">
                         <img t-att-src="props.product.img" alt="element 05" class="mb-1 img-fluid" />
                     </a>
                </div>
            </div>
        </div>
        `
        constructor() {
            super(...arguments);
        }
    };
    
    return ProductCard;
});