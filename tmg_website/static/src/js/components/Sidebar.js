odoo.define("tmg_website.Sidebar", function(require) {
    'use strict';
    
    const { Component } = owl;
    const { xml } = owl.tags;
    const Link = owl.router.Link;
    
    class SideBar extends Component {
        static template = xml`
            <div class="sidebar">
                <div class="top-row pl-4 navbar navbar-dark">
                    <Link class="navbar-brand" to="'HOME'">TMG Demo</Link>
                    <button class="navbar-toggler">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse">
                    <ul class="nav flex-column">
                        <li class="nav-item px-3">
                            <Link to="'HOME'" class="nav-link">
                                <span class="oi oi-home"></span> TRANG CHỦ
                            </Link>
                        </li>
                        <li class="nav-item px-3">
                            <a href="about" class="nav-link">
                                <span class="oi oi-person"></span> VỀ CHÚNG TÔI
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        `
        static components = { Link };
        constructor() {
            super(...arguments);
        }
    };

    return SideBar;
}); 