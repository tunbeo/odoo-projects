odoo.define("tmg_website.app", function(require) {
    'use strict';

    const { Component, router } = owl;
    const { xml } = owl.tags;
    const RouteComponent = router.RouteComponent;

    const APP_TEMPLATE = xml/*xml*/ `
        <app>
          <div class="container">
            <RouteComponent />
          </div>
        </app>
    `;
    class App extends Component {
        static components = { RouteComponent};
        static template = APP_TEMPLATE;
    }
    return App;

});