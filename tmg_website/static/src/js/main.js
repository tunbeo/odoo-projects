odoo.define("tmg_website.main", function(require) {
    'use strict';

    const { Component, Store, mount, router, QWeb } = owl;
    const { whenReady } = owl.utils;
    const { useRef, useDispatch, useState, useStore } = owl.hooks;
    const Calculator = require('tmg_website.Calculator');
    const App = require('tmg_website.app');

    const actions = {

    };

    const initialState = {
        products: [],
    };

    // Get current lang from path name
    var langcode_list = ['vi', 'en']
    var pathname = window.location.pathname;
    var current_lang_search = langcode_list.indexOf(pathname.split('/')[1]);
    var lang_segment = current_lang_search >= 0 ? '/' + pathname.split('/')[1] : '';

    const ROUTES = [
        { name: "CALCULATOR", path: lang_segment + '/calculator/{{id}}', component: Calculator },
    ];

    function makeStore() {
        const state = initialState;
        const store = new Store({ state, actions });
        return store;
    }

    async function setup() {
        owl.config.mode = "dev";
        const env = { store: makeStore() };
        env.router = new router.Router(env, ROUTES, { mode: "history" });
        await env.router.start();
        if (env.router.currentRoute) {
            App.env = env;
            mount(App, { target: document.querySelector('main') });
        }
    }

    whenReady(setup)

});

