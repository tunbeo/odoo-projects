odoo.define("tmg_website.Calculator", function(require) {
    'use strict';

    const { Component } = owl;
    const { xml } = owl.tags;
    const Link = owl.router.Link;
    const { useState, useStore, onMounted } = owl.hooks;
    var ajax = require('web.ajax');
    
    const getData = (calculation_data) => {
        // get query string from url
        var query_string = window.location.search;
        // when edit setting update the result (area, totalprice ...)
        query_string = calculation_data.query_string ? calculation_data.query_string : query_string;
        var search_params = new URLSearchParams(query_string);
        var params = {template_id: calculation_data.template_id};
        for(var pair of search_params.entries()) {
           params[pair[0]] = pair[1]
        }
        // Call ajax to get data
        var settings = {
          "url": window.location.origin + "/ds/door",
          "method": "GET",
          "data": params
        };
         ajax.rpc("/ds/door", params).then(function (result) {
           if(!result.doorTables) {
              return
           }
           calculation_data.name = result.name
           calculation_data.totalPrice = result.totalPrice;
           calculation_data.doorTables.Alu = result.doorTables.Alu
           calculation_data.doorTables.Glass = result.doorTables.Glass;
           calculation_data.doorTables.Accessories = result.doorTables.Accessories;
           calculation_data.doorTables.PriceList = result.doorTables.PriceList;
           calculation_data.baseSetting = result.baseSetting;
           calculation_data.advanceSetting = result.advanceSetting;
           calculation_data.base64Img = 'data:image/bmp;base64,'+result.base64Img;
           calculation_data.query_string = query_string;
           calculation_data.directLink = query_string ? window.location.host + window.location.pathname + '?'+query_string : window.location.host + window.location.pathname;
        });
    }

    function useCalculatorData() {
        const calculation_data = useState({
            name: '',
            totalPrice: 0,
            totalSquareMetter: 0,
            message: 'OK',
            doorTables: {
                Alu: [],
                Glass: [],
                Accessories: [],
                PriceList: [],
            },
            baseSetting: [],
            advanceSetting: [],
            base64Img: '',
            template_id: '',
            query_string: '',
            key: '4J9MIRUgvVQ',
            directLink: window.location.host + window.location.pathname + window.location.search
        });
        onMounted(() => {
            $(".rz-tabview-nav.nav-tabs a").click(function() {
                $(this).tab('show');
            });
            
            // get template id
            const pathArray = window.location.pathname.split("/");
            
            calculation_data.template_id = pathArray.pop();
            
            getData(calculation_data);
            
        });
        
        return calculation_data;
    }

    class Calculator extends Component {
        static template = xml`
           <div class="main">
                <div class="content">
                   <h1><t t-esc="calculation_data.name"/></h1>
                   <p>Ứng dụng tính toán cắt cửa: Nhanh &amp; Chính xác. </p>
                   <div>
                      <img t-att-src="calculation_data.base64Img" />
                      <div class="row">
                         <div class="col-xl-12 col-lg-12 py-2">
                            <h4><t t-esc="calculation_data.name"/></h4>
                            <div class="form-inline">
                               <p>Link trực tiếp: <a t-att-href="calculation_data.directLink"><t t-esc="calculation_data.directLink"/></a></p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div>
                      <div class="col-xl-6 col-lg-12 py-2">
                         <div class="row">
                            <div class="col-md-3">
                               <p>Door ID</p>
                            </div>
                            <div class="col-md-4">
                               <input placeholder="Enter Door ID" t-att-value="calculation_data.template_id"/>
                            </div>
                         </div>
                      </div>
                      <div class="rz-tabview">
                         <ul class="rz-tabview-nav nav nav-tabs">
                            <li class=" rz-state-active">
                               <a href="#basic-info-tab" class="active">
                               <span class="rz-tabview-title">THÔNG TIN CƠ BẢN</span>
                               </a>
                            </li>
                            <li class=" rz-state-active">
                               <a href="#setting-info-tab">
                               <span class="rz-tabview-title">CÀI ĐẶT THÔNG SỐ</span>
                               </a>
                            </li>
                         </ul>
                         <div class="rz-tabview-panels tab-content">
                            <div class="rz-tabview-panel tab-pane active fade in show" id="basic-info-tab">
                               <div class="row">
                                  <div class="col-xl-6 col-lg-12 py-2">
                                    <t t-foreach="calculation_data.baseSetting" t-as="row" t-key="row.cell">
                                        <div class="row">
                                            <div class="col-md-4">
                                               <p><t t-esc="row.name"/></p>
                                            </div>
                                            <div class="col-md-4">
                                               <input t-att-value="row.value" t-att-name="row.cell" t-on-change="updateCalcSetting"/>
                                            </div>
                                         </div>
                                    </t>
                                  </div>
                               </div>
                            </div>
                            <div class="rz-tabview-panel tab-pane" id="setting-info-tab">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-12 py-2">
                                       <t t-foreach="calculation_data.advanceSetting" t-as="row" t-key="row.cell">
                                            <div class="row">
                                                <div class="col-md-4">
                                                   <p><t t-esc="row.name"/></p>
                                                </div>
                                                <div class="col-md-4">
                                                   <input t-att-value="row.value" t-att-name="row.cell" t-on-change="updateCalcSetting"/>
                                                </div>
                                             </div>
                                        </t>
                                    </div>
                                </div>
                            </div>
                         </div>
                      </div>
                      <br />
                      <div class="row">
                         <div class="col">
                            <button style="margin: 0 1rem 1rem 0" name="btnLoad" class="btn btn-primary" t-on-click="refreshCalcSetting">Tải / Làm Lại</button>
                            <button style="margin: 0 1rem 1rem 0" class="btn btn-primary">Tính Lại</button>
                            <button style="margin: 0 1rem 1rem 0" class="btn btn-primary">Sản phẩm tương tự</button>
                            <button style="margin: 0 1rem 1rem 0" class="btn btn-primary">Đặt hàng trực tiếp</button>
                         </div>
                      </div>
                   </div>
                   <br />
                   <div class="rz-tabview">
                      <ul role="tablist" class="rz-tabview-nav nav nav-tabs">
                         <li>
                            <a href="#cut-alu-size-tab" class="active">
                                <span class="rz-tabview-title">KÍCH THƯỚC CẮT NHÔM</span>
                            </a>
                         </li>
                         <li>
                            <a href="#cut-glass-size-tab">
                            <span class="rz-tabview-title">KÍCH THƯỚC CẮT KÍNH</span>
                            </a>
                         </li>
                         <li>
                            <a href="#accessories-tab">
                            <span class="rz-tabview-title">PHỤ KIỆN</span>
                            </a>
                         </li>
                         <li>
                            <a href="#price-calculator-tab">
                            <span class="rz-tabview-title">BẢNG TÍNH GIÁ THÀNH</span>
                            </a>
                         </li>
                      </ul>
                      <div class="rz-tabview-panels tab-content">
                         <div class="rz-tabview-panel tab-pane fade in active show" id="cut-alu-size-tab">
                            <table class="table table-striped">
                               <thead class="thead-light">
                                  <tr>
                                     <th>Tên thanh nhôm</th>
                                     <th>Vị trí thanh</th>
                                     <th>Ký hiệu</th>
                                     <th>Góc cắt</th>
                                     <th>Số lượng</th>
                                     <th>Kích thước (mm)</th>
                                  </tr>
                               </thead>
                               <tbody>
                                 <t t-foreach="calculation_data.doorTables.Alu" t-as="row">
                                    <tr>
                                         <td><t t-esc="row.name"/></td>
                                         <td><t t-esc="row.location"/></td>
                                         <td><t t-esc="row.code"/></td>
                                         <td><t t-esc="row.angle"/></td>
                                         <td><t t-esc="row.quantity"/></td>
                                         <td><t t-esc="row.length"/></td>
                                      </tr>
                                  </t>
                               </tbody>
                            </table>
                         </div>
                         
                         <div class="rz-tabview-pane tab-pane" id="cut-glass-size-tab">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Tên kính</th>
                                        <th>Rộng</th>
                                        <th>Cao</th>
                                        <th>Số lượng</th>
                                        <th>Diện tích (m2)</th>
                                        <th>Vị trí</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <t t-foreach="calculation_data.doorTables.Glass" t-as="row">
                                        <tr>
                                             <td><t t-esc="row.name"/></td>
                                             <td><t t-esc="row.width"/></td>
                                             <td><t t-esc="row.height"/></td>
                                             <td><t t-esc="row.quantity"/></td>
                                             <td><t t-esc="row.s"/></td>
                                             <td><t t-esc="row.location"/></td>
                                          </tr>
                                      </t> 
                                </tbody>
                            </table>
                         </div>
                         
                         <div class="rz-tabview-pane tab-pane" id="accessories-tab">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Tên</th>
                                        <th>Mã</th>
                                        <th>ĐVT</th>
                                        <th>Số lượng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <t t-foreach="calculation_data.doorTables.Accessories" t-as="row">
                                        <tr>
                                             <td><t t-esc="row.name"/></td>
                                             <td><t t-esc="row.code"/></td>
                                             <td><t t-esc="row.unit"/></td>
                                             <td><t t-esc="row.quantity"/></td>
                                          </tr>
                                      </t> 
                                </tbody>
                            </table>
                         </div>
                         
                         <div class="rz-tabview-pane tab-pane" id="price-calculator-tab">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Tên</th>
                                        <th>ĐVT</th>
                                        <th>Số lượng</th>
                                        <th>Đơn giá</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <t t-foreach="calculation_data.doorTables.PriceList" t-as="row">
                                       <tr>
                                            <td><t t-esc="row.name"/></td>
                                            <td><t t-esc="row.unit"/></td>
                                            <td><t t-esc="row.quantity"/></td>
                                            <td><t t-esc="row.price"/></td>
                                            <td><t t-esc="row.sum"/></td>
                                        </tr>
                                     </t> 
                                </tbody>
                            </table>
                         </div>
                         
                      </div>
                   </div>
                </div>
            </div>
        `

        static components = { Link };

        products = useStore((state) => state.products);

        constructor() {
            super(...arguments);
        }

        get displayedProducts() {
            return this.products;
        }
        
        updateCalcSetting(ev) {
            // when user edit the input to change the setting to get result like (area, totalprice ...)
            var elem = $(ev.target);
            var name = elem.attr('name');
            var value=elem.val();
            var params = new URLSearchParams(this.calculation_data.query_string);
            params.set(name, value);
            var query_string = params.toString();
            this.calculation_data.query_string = query_string;
            getData(this.calculation_data);
        }
        
        refreshCalcSetting(ev) {
            this.calculation_data.query_string = '';
            this.calculation_data.directLink = window.location.host + window.location.pathname;
            getData(this.calculation_data);
        }

        calculation_data = useCalculatorData()

    };

    return Calculator;
});
