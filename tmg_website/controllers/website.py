import json
import requests
import logging

from odoo import http
from odoo.addons.website.controllers.main import Website

_logger = logging.getLogger(__name__)


class Website(Website):
    """
    Exetend or replace the route front end website in odoo
    """
    @http.route('/calculator/<string:template>', type='http', auth='public', website=True)
    def calculator(self, **kw):
        return  http.request.render('website.layout', {'title': 'TMG Demo'})
        
        
    @http.route('/doorbrand', type='http', auth='public', website=True)
    def doorbrand(self, **kw):
        doorbrands = http.request.env['adg.doorbrand'].sudo().search([])
        return http.request.render('tmg_website.doorbrand', {'title': 'TMG Demo', 'doorbrands': doorbrands})
    
    @http.route('/counter/<string:alu_rootid>', type='http', auth='public', website=True)
    def counter(self, **kw):
        alu_rootid = kw.get('alu_rootid', '')
        template = 'tmg_website.door'
        doors = http.request.env['adg.door'].sudo().search([['alu_rootid', '=', alu_rootid]])
        if not len(doors):
            template = 'website.page_404'
        return http.request.render(template, {'title': 'TMG Demo', 'doors': doors})
    
    @http.route('/ds/door', type='json', auth='public', website=True, csrf=False)
    def ds_door(self, **kw):
        template_id = kw.get('template_id', '')
        kw.pop('template_id', None)
        key = '4J9MIRUgvVQ'
        kw.update({'key': '4J9MIRUgvVQ'})
        url = 'http://115.146.121.182/ds/door/' + template_id
        try:
            headers = {'User-Agent': 'Odoo (http://www.odoo.com/contactus)'}
            response = requests.get(url, headers=headers, params=kw)
            _logger.info('calling door calculation')
            if response.status_code != 200:
                _logger.error('Request to API failed.\nCode: %s\nContent: %s' % (response.status_code, response.content))
            result = response.json()
        except Exception as e:
            return {}
            
        return response.json()

