# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Wizard(models.TransientModel):

    _name = 'austdoor.wizard'
    _description = "Wizard: Quick Registration of Attendees to Sessions"

    state = fields.Selection([
            ('step1', 'Step 1'),
            ('step2', 'Step 2'),
            ('step3', 'Step 3'),
            ],default='step1',string="Trạng thái")

    def _default_sessions(self):
        return self.env['austdoor.session'].browse(self._context.get('active_ids'))

    session_ids = fields.Many2many('austdoor.session',
        string="Sessions", required=True, default=_default_sessions)
    attendee_ids = fields.Many2many('res.partner', string="Attendees")

    field1 = fields.Char(string="Field 1")
    field2 = fields.Char(string="Field 2")
    field3 = fields.Char(string="Field 3")

    # @api.multi
    def subscribe(self):
        for session in self.session_ids:
            session.attendee_ids |= self.attendee_ids
        return {}

    def action_next1(self):

        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'view_mode': 'form',
            'res_id': this.id,
            'views': [(False, 'form')],
            'target': 'new',
             }

    def action_next2(self):

        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'view_mode': 'form',
            'res_id': this.id,
            'views': [(False, 'form')],
            'target': 'new',
             }

    def action_previous(self):

        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'view_mode': 'form',            
            'res_id': this.id,
            'views': [(False, 'form')],
            'target': 'new',
             }
