import logging

from odoo import models, fields, api

class AustdoorAccount(models.Model):
    _name = 'austdoor.account'
    @api.model
    def _default_employee_id(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    def _default_user_id(self):
        context = self._context
        current_uid = context.get('uid')
        #user = self.env['res.users'].browse(current_uid)
        return current_uid

    name = fields.Char(string="Title")
    active = fields.Boolean(default=True,string="Đang hoạt động")
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, readonly=True,  default=_default_employee_id, domain=lambda self: self._get_employee_id_domain())
    user_id = fields.Many2one('hr.employee', string="Employee", required=True, readonly=True,  default=_default_user_id)
