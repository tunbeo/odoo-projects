# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

#=======================================================================
# base cua cuon
class ADG_OrderCuaCuon(models.Model):#tuong duong voi sale.order
    # Nhung thuoc tinh chung
    #===========================
    _name = 'austdoor.cuacuon'
    _order = "write_date desc"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    name = fields.Char(string="Mã đặt hàng", default="/", readonly=True)
    fullname = fields.Char(string="Tên cửa", track_visibility='onchange')

    state = fields.Selection([
            ('bannhap', 'Bản nháp'),
            ('xacnhan', 'Đại lý xác nhận'),
            ('chapnhan', 'Đã gửi đi'),
            ('sanxuat', 'Đang sản xuất'),
            ('shipping', 'Đang giao hàng'),
            ('done', 'Hoàn thành'),
            ],default='bannhap',string="Trạng thái", track_visibility='onchange')
    description = fields.Text(string="Ghi chú", track_visibility='onchange')
    SoBoCua = fields.Integer(string="Số bộ cửa", default=1, track_visibility='onchange')
    LoCuon = fields.Selection(
        [('t', 'Trong'), ('n', 'Ngoài')],
        'Lô Cuốn', default="t", required=True, track_visibility='onchange')
    CaoPhuBi = fields.Integer(string="Cao phủ bì (mm)", required=True, track_visibility='onchange')
    RongPhuBi = fields.Integer(string="Rộng phủ bì (mm)", required=True, track_visibility='onchange')
    DienTich = fields.Float(string="Diện tích (m2)", digits=(12,3), compute='_compute_dientich', store=True)
    ViTriBoToi = fields.Selection(
        [('t', 'Trái'), ('p', 'Phải')],
        'Vị trí lắp', default="t", required=True, track_visibility='onchange')
    Ray_KichThuoc = fields.Integer(string="Kích thước ray (mm)", required=True, track_visibility='onchange')
    Ray_SoThanh = fields.Integer(string="Số thanh", required=True, track_visibility='onchange')

    NgayGiaoHang = fields.Date(string="Ngày giao hàng",default=fields.Date.today, track_visibility='onchange')

    Certificate_CQ = fields.Boolean(default=False,string="Cấp chứng nhận CQ", track_visibility='onchange')
    Certificate_CO = fields.Boolean(default=False,string="Cấp chứng nhận CO", track_visibility='onchange')

    # khi chon dong cua se quyet dinh cac thuoc tinh tiep theo
    DongCua_Id = fields.Many2one('austdoor.cuacuon.dongcua', ondelete='cascade', #cascade ở đây có ảnh hưởng gì ko ?
        required=True, string="Dòng Cửa", index=True, track_visibility='onchange')
    LoaiCua_Id = fields.Many2one('austdoor.cuacuon.loaicua', ondelete='cascade',
        required=True, string="Loại Cửa", index=True, track_visibility='onchange')
    MauCua_Id = fields.Many2one('austdoor.cuacuon.maucua', ondelete='cascade',
        string="Màu Cửa", index=True, track_visibility='onchange')
    BoToi_Id = fields.Many2one('austdoor.cuacuon.botoi', ondelete='cascade',
        required=True, string="Bộ tời", index=True, track_visibility='onchange')
    Ray_Id = fields.Many2one('austdoor.cuacuon.accessories', ondelete='cascade',
        required=True, string="Loại Ray", index=True, track_visibility='onchange',
        domain=[('loaiphukien_id', '=', 1)])# ray co loai phu kien ID = 1
    Partner_Id = fields.Many2one('res.partner', string="Cửa cuốn đã đặt")
    
    DongSP = fields.Text(string='DongSP', readonly=False)

    #This function is triggered when the user clicks on the button 'Set to concept'
    # @api.one
    def bannhap_progressbar(self):
        self.write({
            'state': 'bannhap',
        })

    # @api.one
    def xacnhan_progressbar(self):
        self.write({
    	'state': 'xacnhan'
        })
    # @api.one
    def chapnhan_progressbar(self):
        self.write({
    	'state': 'chapnhan'
        })

    #cam xoa khi record ko phai la bannhap
    # @api.multi odoo14 obsolated
    def unlink(self):
        for order in self:
            if order.state not in ('bannhap'):
                raise UserError(_('You can not delete a confirmed order. You must first draft it. (Bạn không thể xóa dữ liệu khi trạng thái không phải là bản nháp)'))
        return super(ADG_OrderCuaCuon, self).unlink()

    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].get('austdoor.cuacuon.seq') or ' '
        res = super(ADG_OrderCuaCuon, self).create(values)
        return res

    @api.onchange('LoaiCua_Id')
    def _loaicua_onchange(self):
        res = {}
        res['domain']={'MauCua_Id':[('loaicua_id', '=', self.LoaiCua_Id.id)]}
        return res

    @api.onchange('MauCua_Id')
    def _maucua_onchange(self):
        res = {}
        res['domain']={'GhepMau_Id':[('loaicua_id', '=', self.LoaiCua_Id.id)]}
        return res

    @api.onchange('CaoPhuBi','RongPhuBi')
    def _tinhdientich(self):
        if self.CaoPhuBi and self.RongPhuBi:
            self.DienTich = self.CaoPhuBi * self.RongPhuBi / (1000 * 1000)
            _my_object = self.env['austdoor.cuacuon']
            _my_object.write({'DienTich' : self.DienTich})
        else:
            self.DienTich = 0
    @api.depends('CaoPhuBi','RongPhuBi')
    def _compute_dientich(self):
        for record in self:
            record.DienTich = record.CaoPhuBi * record.RongPhuBi / (1000 * 1000)

    # End Nhung thuoc tinh chung
    #===========================

    #===========================
    # Start Cac thuoc tinh lua chon
    is_show_LuaChonThem = fields.Boolean(default=False)
    Truc114 = fields.Boolean(string="Trục 114",track_visibility='onchange') #done
    is_show_Truc114 = fields.Boolean(default=False)
    GiaDo_Id = fields.Many2one('austdoor.cuacuon.accessories', ondelete='cascade',
        string="Giá đỡ", index=True, track_visibility='onchange',
        domain=[('loaiphukien_id', '=', 2)])
    is_show_GiaDo_Id = fields.Boolean(default=False)
    GiaDo_SoBo = fields.Integer(string="Số bộ giá đỡ", default=0,
        required=True, track_visibility='onchange')
    is_show_GiaDo_SoBo = fields.Boolean(default=False)
    MauPC = fields.Selection(
        [('m1', 'M1'), ('m2', 'M2'), ('m3', 'M3'), ('m4', 'M4'),('m5', 'M5'),('m6', 'M6')],
        'Mẫu PC', default="m1", required=True, track_visibility='onchange')
    is_show_MauPC = fields.Boolean(default=False)
    DoCapLapPC = fields.Integer(string="Độ cao lắp PC (mm)",default=0,
        required=True, track_visibility='onchange')
    is_show_DoCapLapPC = fields.Boolean(default=False)
    ChieuDaiTruc = fields.Integer(string="Chiều Dài Trục (mm)",default=0, required=True, track_visibility='onchange')
    is_show_ChieuDaiTruc = fields.Boolean(default=False)
    LoaiTrucCua_Id = fields.Many2one('austdoor.cuacuon.accessories', ondelete='cascade',
        string="Trục Cửa", index=True, track_visibility='onchange',
        domain=[('loaiphukien_id', '=', 3)])
    is_show_LoaiTrucCua_Id = fields.Boolean(default=False)

    GhepMau_Id = fields.Many2one('austdoor.cuacuon.ghepmau', #done
        ondelete='cascade', string="Ghép Màu", index=True, track_visibility='onchange')
    is_show_GhepMau_Id = fields.Boolean(default=False)
    DoCaoLoThoang = fields.Integer(string="Độ cao lỗ thoáng (mm)",default=0, required=True, track_visibility='onchange')
    is_show_DoCaoLoThoang = fields.Boolean(default=False)

    ChieuDaiKhungHKT = fields.Integer(string="Chiều dài khung HKT (mm)",default=0,track_visibility='onchange')
    is_show_ChieuDaiKhungHKT = fields.Boolean(default=False)

    # End Cac thuoc tinh lua chon
    #===========================

    # da khai bao o tren
    #DongCua_Id = fields.Many2one('austdoor.cuacuon.dongcua', ondelete='cascade',
    #    required=True, string="Dòng Cửa", index=True, track_visibility='onchange')

    orderline_ids = fields.One2many('austdoor.cuacuon.orderline','cuacuon_id', string="Phụ kiện đi kèm", track_visibility='onchange')
    #phukiendongcua_id = fields.Many2one('austdoor.phukiendongcua')

    total_amount = fields.Float(string="Thành tiền", readonly=True, compute='_compute_order_amount')
    discount_amount = fields.Float(string="Chiết khấu", compute='_compute_order_amount')
    total_paid_amount = fields.Float(string="Số tiền phải trả", compute='_compute_order_amount')


    def _compute_line_data_for_dongcua_change(self, line):
        return {
            'name': line.accessories_id.fullname,
            'accessories_id': line.accessories_id,
            'quantity': 0,
            'price': line.accessories_id.price_banbuon,
            'amount': 0,
        }

    #@api.one
    def _compute_order_amount(self):
        for rec in self:
            amount_loaicua = rec.LoaiCua_Id.price_banbuon * rec.DienTich
            amount_maucua = rec.MauCua_Id.price_banbuon * rec.DienTich
            amount_ghepmau = rec.GhepMau_Id.price_banbuon * rec.DienTich
            amount_botoi = rec.BoToi_Id.price_banbuon
            amount_ray = ((rec.Ray_Id.price_banbuon * rec.Ray_KichThuoc)/1000) * rec.Ray_SoThanh
            amount_giado = rec.GiaDo_Id.price_banbuon * rec.GiaDo_SoBo
            #_logger.error(loaicua_amount)
            total_phukien = 0.0
            for order in rec:
                for line in order.orderline_ids:
                    total_phukien += line.amount
                rec.total_amount = total_phukien + amount_loaicua + amount_maucua + amount_ghepmau + amount_botoi + amount_ray + amount_giado
                rec.total_paid_amount = rec.total_amount-rec.discount_amount

    @api.onchange('DongCua_Id')
    def _dongcua_onchange(self):#khi nguoi dung thay doi Dòng cửa
        res = {}
        dongcua = self.DongCua_Id

        self.DongSP = int(self.DongCua_Id.dongsp_id)
        #_logger.error(self.DongSP)
        order_lines = [(5, 0, 0)]
        for line in dongcua.phukiendongcua_ids:#lay tung phu kien cua dong cua
            #_logger.error(line)                         #austdoor.phukiendongcua(1,)
            #_logger.error(line.accessories_id)          #austdoor.cuacuon.accrssories(153)
            #_logger.error(line.accessories_id.name)     #AD9

            data = self._compute_line_data_for_dongcua_change(line)
            order_lines.append((0, 0, data))
        self.orderline_ids = order_lines


        #giado = self.env['austdoor.cuacuon.accessories'].search([('loaiphukien_id','=',2), ('dongsp_id','=',self.DongSP.id)])
        #self.GiaDo_Id = giado








        #set Tat ca bang false
        self.is_show_Truc114 = False
        self.is_show_GhepMau_Id = False
        self.is_show_DoCaoLoThoang = False
        self.is_show_GiaDo_Id = False
        self.is_show_GiaDo_SoBo = False
        self.is_show_ChieuDaiKhungHKT = False
        self.is_show_MauPC = False
        self.is_show_DoCapLapPC = False
        self.is_show_LoaiTrucCua_Id = False
        self.is_show_ChieuDaiTruc = False


        self.is_show_LuaChonThem = False


        #self.name = self.DongCua_Id.name
        #================================================
        # Start cac thuoc tinh cua Cua cuon thep tam lien
        if self.DongCua_Id.id == 1:
            self.is_show_Truc114 = True
            self.is_show_GhepMau_Id = True
            self.is_show_DoCaoLoThoang = True
            self.is_show_GiaDo_Id = True
            self.is_show_GiaDo_SoBo = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon thep tam lien
        #================================================


        #================================================
        # Start cac thuoc tinh cua Cua cuon Nan Nhom Aluroll
        if self.DongCua_Id.id == 2:
            self.is_show_MauPC = True
            self.is_show_DoCapLapPC = True
            self.is_show_ChieuDaiTruc = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon Nan Nhom Aluroll
        #================================================

        #================================================
        # Start cac thuoc tinh cua Cua cuon Nan Nhom Truyen thong
        if self.DongCua_Id.id == 3:
            self.is_show_DoCapLapPC = True
            self.is_show_LoaiTrucCua_Id = True
            self.is_show_ChieuDaiTruc = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon Nan Nhom Truyen thong
        #================================================

        #================================================
        # Start cac thuoc tinh cua Cua cuon Gara
        if self.DongCua_Id.id == 4:
            # self.is_show_UPSP1000 = True
            # self.is_show_TayDKTXDK1 = True
            self.is_show_GiaDo_Id = True
            self.is_show_GiaDo_SoBo = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon Gara
        #================================================

        #================================================
        # Start cac thuoc tinh cua Cua cuon Trung tam thuong mai
        if self.DongCua_Id.id == 5:
            self.is_show_LoaiTrucCua_Id = True
            self.is_show_ChieuDaiTruc = True
            self.is_show_GiaDo_Id = True
            self.is_show_GiaDo_SoBo = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon Trung tam thuong mai
        #================================================

        #================================================
        # Start cac thuoc tinh cua Cua cuon Cong nghiep
        if self.DongCua_Id.id == 6:
            self.is_show_ChieuDaiKhungHKT = True

            self.is_show_LuaChonThem = True
        # End cac thuoc tinh cua Cua cuon Cong nghiep
        #================================================
        res['domain'] = {
            'LoaiCua_Id': [('dongcua_id', '=', self.DongCua_Id.id)],
            'GiaDo_Id': [('loaiphukien_id', '=', 2), ('dongsp_id', '=', int(self.DongSP))],
            'LoaiTrucCua_Id': [('loaiphukien_id', '=', 3),('dongsp_id', '=', int(self.DongSP))],
            'Ray_Id': [('loaiphukien_id', '=', 1),('dongsp_id', '=', int(self.DongSP))],
            'BoToi_Id': [('dongsp_id', '=', int(self.DongSP))],
             }
        return res
