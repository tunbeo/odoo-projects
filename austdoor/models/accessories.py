# -*- coding: utf-8 -*-
from odoo import models, fields, api

#base accrssories
class AustdoorAccessory(models.Model):
    _name = 'austdoor.cuacuon.accessories'
    name = fields.Char(string="Tên", required=True)#model
    fullname = fields.Char(string="Tên đầy đủ")#ten goi
    dongsp_id = fields.Many2one('austdoor.cuacuon.dongsp',
        string="Dòng SP", index=True)
    price = fields.Float(string="Giá bán lẻ (chưa VAT)", default=0,digits=(12,0))
    price_banbuon = fields.Float(string="Giá bán buôn theo tiêu chuẩn (có VAT)", default=0,digits=(12,0))
    price_banbuon_db = fields.Float(string="Giá bán buôn theo tiêu chuẩn đồng bộ (có VAT)", default=0,digits=(12,0))
    code = fields.Char(string="Mã vật tư")
    description = fields.Char(string="Mô tả")
    donvitinh = fields.Char(string="Đơn vị tính")
    tieuchuan_sp_db = fields.Char(string="Tiêu chuẩn sản phẩm đồng bộ")
    loaiphukien_id = fields.Many2one('austdoor.cuacuon.loaiphukien', ondelete='cascade',
        string="Loại Phụ kiện", index=True)
    active = fields.Boolean(default=True,string="Đang hoạt động")

class CuaCuon_LoaiPhuKien(models.Model):
    _name = 'austdoor.cuacuon.loaiphukien'
    name = fields.Char(string="Tên", required=True)

class CuaCuon_DongSP(models.Model):
    _name = 'austdoor.cuacuon.dongsp'
    name = fields.Char(string="Tên", required=True)

class CuaCuon_DongCua(models.Model):
    _name = 'austdoor.cuacuon.dongcua'
    name = fields.Char(string="Tên", required=True)
    code = fields.Char(string="Mã dòng SP", required=True)
    dongsp_id = fields.Many2one('austdoor.cuacuon.dongsp', ondelete='cascade',
        string="Dòng SP", index=True)
    phukiendongcua_ids = fields.One2many('austdoor.phukiendongcua','dongcua_id',string="Phụ kiện cho dòng cửa")

#cai nay chinh la : SaleOrderTemplateOption
class PhukienDongcua(models.Model):
    _name = 'austdoor.phukiendongcua'
    _order = 'sequence, id'
    sequence = fields.Integer(string='Sequence', default=100)
    dongcua_id = fields.Many2one('austdoor.cuacuon.dongcua', ondelete='cascade',
        string="Dòng cửa", index=True)
    accessories_id = fields.Many2one('austdoor.cuacuon.accessories', ondelete='cascade',
        string="Phụ kiện đi kèm", index=True)

class ADG_Partner(models.Model):
    _inherit = 'res.partner'
    # Add a new column to the res.partner model, by default partners are not
    # instructors
    LaDaiLyAustdoor = fields.Boolean("Là đại lý của Austdoor", default=False)
    #cuacuon_ids = fields.One2many('austdoor.cuacuon', string="Cửa cuốn")

class ADG_OrderLine(models.Model):
    _name = 'austdoor.cuacuon.orderline'
    _description = 'Phụ kiện của đặt hàng cửa cuốn'
    _order = 'sequence, id'
    name = fields.Char(string="Tên", store=True,related='accessories_id.fullname')
    sequence = fields.Integer(string='Sequence', default=100)
    #lien quan den dat hang cho cua cuon xac dinh
    cuacuon_id = fields.Many2one('austdoor.cuacuon', ondelete='cascade',
        string="Trong đặt hàng cửa cuốn", index=True)
    accessories_id = fields.Many2one('austdoor.cuacuon.accessories', ondelete='cascade',
        string="Mã phụ kiện", index=True, required=True)
    amount = fields.Float(string="Thành tiền",store=True)
    quantity = fields.Integer(string='Số lượng', default=0)
    donvitinh = fields.Char(string="ĐVT", store=True,related='accessories_id.donvitinh')
    price = fields.Float(string='Đơn giá',store=True , related='accessories_id.price_banbuon')

    @api.onchange('quantity','amount','price','accessories_id')
    def _quantity_onchange(self):
        self.amount = self.quantity * self.price
        _my_object = self.env['austdoor.cuacuon.orderline']
        _my_object.write({'amount' : self.amount})



















class CuaCuon_LoaiCua(models.Model):
    _name = 'austdoor.cuacuon.loaicua'
    _inherit = ['austdoor.cuacuon.accessories']
    dongcua_id = fields.Many2one('austdoor.cuacuon.dongcua', ondelete='cascade',
        string="Thuộc dòng cửa", index=True)
    tieuchuan_sp = fields.Char(string="Tiêu chuẩn sản phẩm")
    tieuchuan_sp_db = fields.Char(string="Tiêu chuẩn sản phẩm đồng bộ")
    ray_tieuchuan = fields.Char(string="Ray tiêu chuẩn theo cửa")
    dong_sp = fields.Char(string="Dong SP")
    #botoi_ids = fields.Many2many('austdoor.cuacuon.botoi', string="Bộ tời tương thích")


class CuaCuon_MauCua(models.Model):
    _name = 'austdoor.cuacuon.maucua'
    _inherit = ['austdoor.cuacuon.accessories']
    loaicua_id = fields.Many2one('austdoor.cuacuon.loaicua', ondelete='cascade',
        string="Cho Loại Cửa", index=True)

class CuaCuon_GhepMau(models.Model):
    _name = 'austdoor.cuacuon.ghepmau'
    _inherit = ['austdoor.cuacuon.accessories']
    loaicua_id = fields.Many2one('austdoor.cuacuon.loaicua', ondelete='cascade',
        string="Cho Loại Cửa", index=True)

class CuaCuon_BoToi(models.Model):
    _name = 'austdoor.cuacuon.botoi'
    _inherit = ['austdoor.cuacuon.accessories']
    #loaicua_ids = fields.Many2many('austdoor.cuacuon.loaicua', string="Cửa tương thích")
