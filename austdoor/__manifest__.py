# -*- coding: utf-8 -*-
{
    'name': "Austdoor",

    'summary': """
        Đặt hàng cửa cuốn và phụ kiện Austdoor:
            - Cửa cuốn thép tấm liền
            - Cửa cuốn thép tấm liền
            - Cửa cuốn thép tấm liền
        """,

    'description': """
        App đặt hàng công ty Austdoor
    """,

    'author': "TT",
    'website': "http://www.austdoor.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Test',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','website','mail'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/cuacuon.xml',
        'views/partner.xml',
        'views/menu.xml',
        'views/austdoor_template.xml',
        'views/assets_backend.xml',
        'views/web_home_snippets.xml',
        'views/cuacuon_seq.xml',
        'report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
