# -*- coding: utf-8 -*-
from odoo import http

class austdoor(http.Controller):
     @http.route('/austdoor/austdoor/', auth='public')
     def index(self, **kw):
         return "Hello, world"

     @http.route('/austdoor/austdoor/objects/', auth='public')
     def list(self, **kw):
         return http.request.render('austdoor.listing', {
             'root': '/austdoor/austdoor',
             'objects': http.request.env['austdoor.austdoor'].search([]),
         })

     @http.route('/austdoor/austdoor/objects/<model("austdoor.austdoor"):obj>/', auth='public')
     def object(self, obj, **kw):
         return http.request.render('austdoor.object', {
             'object': obj
         })
