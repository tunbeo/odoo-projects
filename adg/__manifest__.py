# -*- coding: utf-8 -*-
{
    'name': "ADG",

    'summary': """ADG Core App""",
    'sequence': -99,
    'description': """
        ADG module for managing trainings:
            - training courses 
            - training sessions
            - attendees registration
    """,

    'author': "TMG",
    'website': "http://www.tmgsolution.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board','mail'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/adg.xml',
        'views/door.xml',
        'views/course.xml',
        'views/session.xml',
        'views/partner.xml',
        'views/session_board.xml',
        'views/reports.xml',
        'views/wizard.xml',
        'demo/demo.xml',
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
    'installable': True,
    'application': True,
}
