from odoo import models, fields, api, exceptions, _

class doorcategory(models.Model):
    _name = 'adg.doorcategory'
    _description = 'Nhóm Cửa'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "sort,id asc"

    name = fields.Char(string='Tên', required=True, track_visibility='onchange')
    desc = fields.Char(string='Mô tả', required=False, track_visibility='onchange')

    active1 = fields.Boolean(string="Active", default=True, track_visibility='onchange')
    sort = fields.Integer(string='Thứ tự', default=100, required=False, track_visibility='onchange')

class doorsubcategory(models.Model):
    _name = 'adg.doorsubcategory'
    _description = 'Nhóm Cửa Phụ'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string='Tên', required=True, track_visibility='onchange')
    desc = fields.Char(string='Mô tả', required=False, track_visibility='onchange')

    active1 = fields.Boolean(string="Active", default=True, track_visibility='onchange')
    sort = fields.Integer(string='Thứ tự', default=100, required=False, track_visibility='onchange')