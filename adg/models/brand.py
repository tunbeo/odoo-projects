from odoo import models, fields, api, exceptions, _

class brand(models.Model):
    _name = 'adg.brand'
    _description = 'Alu Brand'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "sort,id asc"

    name = fields.Char(string='Tên', required=True, track_visibility='onchange')
    logo = fields.Char(string='Logo', required=False, track_visibility='onchange')

    active1 = fields.Boolean(string="Active", default=True, track_visibility='onchange')
    sort = fields.Integer(string='Thứ tự', default=100, required=False, track_visibility='onchange')