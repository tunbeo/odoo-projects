from odoo import models, fields, api, exceptions, _

class doorbrand(models.Model):
    _name = 'adg.doorbrand'
    _description = 'Hệ nhôm'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "sort,id asc"

    name = fields.Char(string='Tên', required = True, track_visibility='onchange')
    brand = fields.Many2one('adg.brand', string="Brand", required=True, track_visibility='onchange')
    root_id = fields.Char(string='ID',required=False, track_visibility='onchange')
    type = fields.Char(string='Kiểu',required=False, track_visibility='onchange')
    description = fields.Char(string='Mô tả',required=False, track_visibility='onchange')
    imagePath = fields.Char(string='Ảnh',required=False, track_visibility='onchange')
    attachFilePath = fields.Char(string='File đính kèm',required=False, track_visibility='onchange')



    favorites_by_ids = fields.Many2many('res.partner')
    active1 = fields.Boolean(string = "Active", default=True, track_visibility='onchange')
    sort = fields.Integer(string='Thứ tự', default=100, required=False, track_visibility='onchange')

