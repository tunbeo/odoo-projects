from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError

class door(models.Model):
    _name = 'adg.door'
    _description = 'Mẫu Cửa'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "sort,id asc"

    name = fields.Char(string='Tên', required=True, track_visibility='onchange')

    brand_id = fields.Many2one('adg.brand', string="Hãng nhôm", required=False, track_visibility='onchange')
    doorbrand_id = fields.Many2one('adg.doorbrand', string="Hệ nhôm", required=False, track_visibility='onchange')
    category_id = fields.Many2one('adg.doorcategory', string="Nhóm cửa", required=False, track_visibility='onchange')
    subcategory_id = fields.Many2one('adg.doorsubcategory', string="Nhóm cửa phụ", required=False, track_visibility='onchange')

    root_id = fields.Char(string='ID gốc', required=False, track_visibility='onchange')
    excelFilePath = fields.Char(string='Excel File Name', track_visibility='onchange')
    imageFilePath = fields.Char(string="Image File Name", required=False, track_visibility='onchange')

    active1 = fields.Boolean(string="Active", default=True, track_visibility='onchange')
    sort = fields.Integer(string='Thứ tự', default=100, required=False, track_visibility='onchange')

    @api.constrains('imageFilePath')
    def _check_imageFilePath(self):
        for r in self:
            existing_record = self.search([('imageFilePath', '=', r.imageFilePath), ('id', '!=', r.id)])
            if existing_record:
                raise ValidationError(_("imageFilePath đã trùng"))




























